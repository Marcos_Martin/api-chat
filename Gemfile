source 'https://rubygems.org'

git_source(:github) do |repo_name|
  repo_name = "#{repo_name}/#{repo_name}" unless repo_name.include?("/")
  "https://github.com/#{repo_name}.git"
end


# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '~> 5.1.6'
# Use Puma as the app server
gem 'puma', '~> 3.7'
# MongoDb as data base
gem 'mongoid', '~> 7.0.1'
# Paginate views for MongoDB
gem 'will_paginate_mongoid', '~> 2.0.1'
# A pure ruby implementation of the RFC 7519 OAuth JSON Web Token (JWT) standard.
gem 'jwt', '~> 2.1.0'
# ActiveModelSerializers is undergoing some renovations
gem 'active_model_serializers', '~> 0.10.0'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
# gem 'jbuilder', '~> 2.5'
# Use Redis adapter to run Action Cable in production
gem 'redis', '~> 4.0'
# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.7'
# Use Rack CORS for handling Cross-Origin Resource Sharing (CORS), making cross-origin AJAX possible
gem 'rack-cors', '~> 1.0.2', require: 'rack/cors'

group :development, :test do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen'
  # Extra help in rails console
  #  Use "binding.pry" for execution break and debugging
  #  step: next line and step 2 advances two lines
  #  next : next 2 advances two lines in the same frame. next 2 advances two lines
  #  continue: moves the program forward to the end
  #  exit-p: close pry by continuing normal execution
  #  https://github.com/deivid-rodriguez/pry-byebug
  gem 'awesome_rails_console'
  gem 'pry-byebug'
  gem 'pry-stack_explorer'


  # gems for testing
  gem 'rspec-rails'
  gem 'factory_bot_rails', '~> 4.8.2'
  gem 'shoulda-matchers'
  gem 'database_cleaner'
  gem 'action-cable-testing'
  gem 'json_spec'
  gem 'faker'
  gem 'mongoid-rspec'


end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]

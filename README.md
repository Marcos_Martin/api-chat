# Descripción 
El objetivo de esta prueba es realizar una aplicación web de chat que permita a 
distintos usuarios acceder a diferentes salas de chat y poder comunicarse con 
otros usuarios de esa sala en tiempo real. 
Tecnologías 
Se deberán de utilizar las siguientes tecnologías: 

- Ruby on Rails 
- Action Cable 
- Vuejs 
- Rspec 
- Mongoid (opcional) 

###### Requisitos 
- Al acceder al chat se pedirá un nombre de usuario. 
- Al introducir el nombre de usuario se accederá a una pantalla con un listado de 
Salas. 
- Cualquier usuario podrá crear nuevas salas. 
- Al entrar a una sala se mostrará un chat con los últimos 20 mensajes. 
- El chat deberá recibir los mensajes en tiempo real de todos los usuarios 
conectados a la sala. 
- El código se subirá a Github con una descripción en el README.md de todo lo 
necesario para poder ejecutarlo. 

## Servidor Api Rest :computer:
El servidor necesita tener instalado Redis y MongoDB. Por simplicidad
se puede usar Rvm para gestionar las gemas.   
Para ejecutar la aplicación:

``` 
1.  gem install bundle
2.  bundle
3.  rails s
```
Para tratar con la aplicación por consola, usaremos https://httpie.org/  
Estas son las llamadas que manejaremos:  

``` 
1. Crear un nuevo usuario y un token para identificarlo es posteriores peticiones
  http :3000/signup username=marcos password=asd
2. Obtener token de autenticación
  http :3000/auth/login username=marcos password=asd
3. Obtener todas las salas de chat
  http :3000/rooms Authorization:"token de autenticación"
4. Crear una sala para un usuario autenticado
  http --form POST :3000/rooms Accept:'application/vnd.rooms.v1+json' Authorization:"token de autenticación" title="Sala 1"
5. Crear mensaje en una sala
  http --form POST :3000/rooms/"id room"/messages Accept:'application/vnd.messages.v1+json' Authorization:"token de autenticación" content="Mensaje 1"
6. Obtener todos los mensajes de una sala
  http :3000/rooms/"id room"/messages Accept:'application/vnd.messages.v1+json' Authorization:"token de autenticación"
```
Para probar los test ejecutar:  
```bundle exec rspec```


## Cliente Vue.js que consume los recursos que ofrece la aplicación de servidor :art:
El cliente se encuentra en el directorio /CHAT_FRONT 
Es necesario tener instalado npm para Vue.js

``` bash
1. instalar dependencias
npm install

2. ejecutar servidor del cliente en localhost:8080
npm run dev
```

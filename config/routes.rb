Rails.application.routes.draw do
  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'

  scope module: :v1, constraints: ApiVersion.new('v1', true) do
    resources :rooms, only: [:index, :create] do
      resources :messages, only: [:index, :create]
    end
    # Serve websocket cable requests in-process
    mount ActionCable.server => '/cable'
  end
end

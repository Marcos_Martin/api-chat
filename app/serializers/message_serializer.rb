class MessageSerializer < ActiveModel::Serializer
  # attributes to be serialized  
  attributes :id, :content, :user_id
end

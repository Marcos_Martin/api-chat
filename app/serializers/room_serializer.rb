class RoomSerializer < ActiveModel::Serializer
  # attributes to be serialized  
  attributes :id, :title, :user_id
  # model association
  has_many :messages
end

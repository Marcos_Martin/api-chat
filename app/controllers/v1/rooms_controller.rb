class V1::RoomsController < ApplicationController
  before_action :set_room, only: %i[show update destroy]

  def index
    @rooms = Room.all.paginate(page: params[:page], per_page: 10)
    json_response(@rooms)
  end

  def create
    @room = current_user.rooms.create(room_params)
    json_response(@room, :created)
  end

  private

  def room_params
    # whitelist params
    params.permit(:title)
  end

  def set_room
    @room = Room.find(params[:id])
  end
end

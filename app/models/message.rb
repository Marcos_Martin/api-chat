class Message
  # attributes for the document
  include Mongoid::Document
  include Mongoid::Timestamps
  field :room_id, type: Integer
  field :user_id, type: String
  field :content, type: String

  # Model associations
  belongs_to :room
  belongs_to :user

  # Show messages in order
  default_scope { order(created_at: :desc) }

  # Validations
  validates_presence_of :content, :user_id
end

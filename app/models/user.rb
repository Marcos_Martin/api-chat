class User
  # attributes for the document
  include Mongoid::Document
  field :username, type: String
  field :password_digest, type: String

  # encrypt password
  include ActiveModel::SecurePassword
  has_secure_password

  # Model associations
  has_many :rooms
  has_many :messages

  # Validations
  validates_presence_of :username, :password_digest
  validates_uniqueness_of :username
end
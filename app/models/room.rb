class Room
  # attributes for the document
  include Mongoid::Document
  field :title, type: String
  field :user_id, type: String

  # Model associations
  has_many :messages, dependent: :destroy
  belongs_to :user

  # Validations
  validates_presence_of :title, :user_id

end

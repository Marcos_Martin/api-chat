# spec/factories/Messages.rb
FactoryBot.define do
  factory :message do
    content { Faker::Lorem.sentence }
    room_id nil
    user_id nil
  end
end
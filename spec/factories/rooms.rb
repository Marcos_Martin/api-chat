# spec/factories/rooms.rb
FactoryBot.define do
  factory :room do
    title { Faker::Lorem.word }
    user_id { Faker::Number.number(10) }
  end
end
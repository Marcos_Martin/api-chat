# spec/requests/rooms_spec.rb
require 'rails_helper'

RSpec.describe 'Rooms API', type: :request do
  
  # add todos owner
  let(:user) { create(:user) }
  # initialize test data 
  let!(:rooms) { create_list(:room, 10, user_id: user.id) }
  let(:room_id) { rooms.first.id }

  # authorize request
  let(:headers) { valid_headers }

  # Test suite for GET /rooms
  describe 'GET /rooms' do
    # make HTTP get request before each example
    before { get '/rooms', params: {}, headers: headers }

    it 'returns rooms' do
      # Note `json` is a custom helper to parse JSON responses
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end
  end

  # Test suite for POST /rooms
  describe 'POST /rooms' do
    let(:valid_attributes) do
      # send json payload
      { title: 'Learn Vue', createdby: user.id.to_s }.to_json
    end

    context 'when the request is valid' do
      before { post '/rooms', params: valid_attributes, headers: headers }

      it 'creates a room' do
        expect(json['title']).to eq('Learn Vue')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end
  end
end